# infra.containers.image-lib

> Library of building-block container defintions and images

Predictable base containers for other projects to reference in their own definitions.

## Included Images

This project ships the following Docker images from the [packages/](./packages/) directory:

| Image     | Base Image               | Tools                                    | Uses                                  |
| --------- | ------------------------ |----------------------------------------- | ------------------------------------- |
| flagship  | debian:bookworm-slim     | Docker CLI, docker-buildx, Dagger, proto | General development, CI/CD            |
| autobox   | bitnami/minideb:bookworm | Ansible, OpenTofu                        | Machine provisioning, automation jobs |

## Using Images

Tools that use Docker images can pull pre-built images from this project's
[container registry][links.container-registry]:

### Dockerfile

```dockerfile
FROM registry.gitlab.com/alexlab-cloud/infra/containers/image-lib/flagship:0.5.3
...
```

### Compose

```yml
services:
    flagship:
        image: registry.gitlab.com/alexlab-cloud/infra/containers/image-lib/flagship:0.5.3
        ...
```

### Dev Container

In either `.devcontainer.json` OR `.devcontainer/devcontainer.json`:

```json
{
    "image": "registry.gitlab.com/alexlab-cloud/infra/containers/image-lib/flagship:0.5.3"
}
```

## Publishing Images

Container images are built with the CI/CD pipeline. Ensure there is variable named `GITLAB_TOKEN`
containing a Gitlab Access Token with `api`, `read_api`, `read_repository`, `write_repository`, `read_registry`, and
`write_registry` permissions somewhere in the group/project hierarchy.

### Package Structure and Versioning

Each subdirectory of [`packages/`](./packages/) containing a `package.json` is a container image definition.
[Changesets][href.gh.changesets] is used to manage version bumps when packages are updated.

## Resources

1. [www.kenmuse.com: *Docker from Docker in Alpine Dev Containers*][1]
2. [code.visualstudio.com: *Use Docker or Kubernetes from a container*][2]

[1]: https://www.kenmuse.com/blog/docker-from-docker-in-alpine-dev-containers/
[2]: https://code.visualstudio.com/remote/advancedcontainers/use-docker-kubernetes

<!-- Links -->
[links.container-registry]: https://gitlab.com/alexlab-cloud/infra/containers/image-lib/container_registry/
[href.gh.changesets]: https://github.com/changesets/changesets
