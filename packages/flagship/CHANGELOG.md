# @alexlab-cloud/infra.container-lib.flagship

## 0.5.2

### Patch Changes

- 46597bc: Fix missing install for Docker and associated tools

## 0.5.1

### Patch Changes

- d60a0e6: Flagship fixes + working devcontainer using flagship image

## 0.5.0

### Minor Changes

- 6d14a57: Added the autobox image and added small improvements to flagship and the project as a whole

## 0.4.0

### Minor Changes

- This is a meaningful change
- 3a16b28: Reset changesets and re-release with changesets-gitlab
