# @alexlab-cloud/infra.container-lib.autobox

## 0.2.0

### Minor Changes

- 6d14a57: Added the autobox image and added small improvements to flagship and the project as a whole
